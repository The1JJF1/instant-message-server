package client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

class Vlakno extends Thread {
    private OutputStream _output;

    Vlakno(OutputStream output) {
        this._output = output;
    }

    public void run() {
        try {
            byte[] b = new byte[1024];
            int len = 0;
            while (len > -1) {
                len = System.in.read(b, 0, 1024);
                if (len > 0) {
                    this._output.write(b, 0, len);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class TelnetClient {
    public static void main(String[] args) {
        String adres;
        int port = 23;
        if(args.length > 0) {
            adres = args[0];

            if(args.length > 1) {
                try {
                    int p = Integer.parseInt(args[1]);
                    if(p > 0 && p<65536)
                        port = p;
                }catch(Exception e){
                    System.out.println("Zadano spatné číslo portu.\n");
                }
            }
        }else{
            System.out.println("Nezadali jste povinny parametr - adresa");
            return;
        }
        System.out.printf("Conection to %s, port %d\n",adres,port);
        try {
            Socket socket = new Socket(adres,port);
            InputStream input = socket.getInputStream();
            OutputStream output = socket.getOutputStream();

            Vlakno v = new Vlakno(output);
            v.setDaemon(true);
            v.start();

            byte[] b = new byte[1024];
            int len = 0;
            while(len> -1){
                len = input.read(b, 0, 1024);
                if(len > 0){
                    System.out.write(b, 0, len);
                }

            }
            socket.close();
            System.out.println("Socket byl uzavren");


        }catch (UnknownHostException e) {
            System.out.println("Neznamy host\n" + e.getMessage()+"\n");
        } catch (IOException e) {
            System.out.println("Chyba pri navazovani spojeni.\n" + e.getMessage()+"\n");
        }
        // end of main
    }
}